var koreio_8h =
[
    [ "KIO_ANReadBase", "koreio_8h.html#ad37fc9cb1b88d325f1534c4e14b2bd35", null ],
    [ "KIO_ANWriteBase", "koreio_8h.html#a8f02785019ab048b425b77b8402043a0", null ],
    [ "KIO_CANReadBase", "koreio_8h.html#aad2d05bde5c0744a1133e3305aca2e16", null ],
    [ "KIO_CANWriteBase", "koreio_8h.html#a8687b3f64717e4bb22caa3687337ed99", null ],
    [ "KIO_ClearCAN", "koreio_8h.html#acf8fbb1445d9ef5f315a354b08ed9cfa", null ],
    [ "KIO_FreqBase", "koreio_8h.html#ac4f38c20022d6cafd8d299c6a42c493b", null ],
    [ "KIO_FWVersion", "koreio_8h.html#a57779a87b68955fa260085a274677501", null ],
    [ "KIO_I2C_ScanRead", "koreio_8h.html#ab084c19dd3a0ee045f0cd3284af1d958", null ],
    [ "KIO_I2CList", "koreio_8h.html#abdf77fca1fc59980ab482b4bdd700cc9", null ],
    [ "KIO_I2CReadBase", "koreio_8h.html#a419ad1e5cb67dad29b89b69d1139dc78", null ],
    [ "KIO_I2CReturnRead", "koreio_8h.html#ae947b4749d7dea81d6f6e26bc4dec922", null ],
    [ "KIO_I2CScan", "koreio_8h.html#a8033227d2727529120a6f535fa2fb09a", null ],
    [ "KIO_I2CWriteAddr", "koreio_8h.html#ab68bdf98e881da5d72ba656e5fb3564e", null ],
    [ "KIO_I2CWriteBase", "koreio_8h.html#a41664625186b82cdf1ee9c63e33891be", null ],
    [ "KIO_IOChgBase", "koreio_8h.html#aaf93a6d87bf9e83621e677965d0669cd", null ],
    [ "KIO_IOChgLed", "koreio_8h.html#a3527a674cece9cf491c84188f2e7c746", null ],
    [ "KIO_IOClearBase", "koreio_8h.html#a9ecfe64a56322ab999c5a055fb476235", null ],
    [ "KIO_IOConfigIn", "koreio_8h.html#af80bae15b05fab3d8f1e9f914a932c84", null ],
    [ "KIO_IOConfigOut", "koreio_8h.html#a143c90fb472f4a6ad42963008ecee563", null ],
    [ "KIO_IOConfigPwm", "koreio_8h.html#abd1e3855100ce8d5e9fa1358f6685bfd", null ],
    [ "KIO_IOReadBase", "koreio_8h.html#a322509e8250efd58236d4ad0b7dd9071", null ],
    [ "KIO_IOSetBase", "koreio_8h.html#a3ca61568dfdc201a1b6039077fdd5724", null ],
    [ "KIO_PWChgBase", "koreio_8h.html#aedf25c7ed256a4d1289a2b182ff916a3", null ],
    [ "KIO_PWClearBase", "koreio_8h.html#a9d581e1d09611720e3898e8949629bd0", null ],
    [ "KIO_PWMRatio", "koreio_8h.html#a8dc0918a640975c77147779e0544e326", null ],
    [ "KIO_PWSetBase", "koreio_8h.html#a6d2863defc8bfc56b833bb31784c91f0", null ],
    [ "KIO_Status", "koreio_8h.html#a00fded37239212b14d160a66cc82a9e6", null ],
    [ "KIO_TimerBase", "koreio_8h.html#a505ca3a1d5f8743d2aca4557f75e8923", null ],
    [ "kio_ChangeIO", "koreio_8h.html#aefbf593655fa14e6e5bf626afe95edc6", null ],
    [ "kio_ChangeLed", "koreio_8h.html#a0b68a727284f4b3b8b72211f21c2afe1", null ],
    [ "kio_ChangePWM_freq", "koreio_8h.html#a0769d5a8d2be0eb97523a3fed4dc90b7", null ],
    [ "kio_ChangePWM_ratio", "koreio_8h.html#ab6a06a4f584b5e2f954622be0add1025", null ],
    [ "kio_ClearIO", "koreio_8h.html#afa84586af458c590016d494355da876d", null ],
    [ "kio_ConfigIO", "koreio_8h.html#a8de2871a0489627df03903de29ca5121", null ],
    [ "kio_GetFWVersion", "koreio_8h.html#a3b327a8cc44ed05c52a56ae273161384", null ],
    [ "kio_i2c_ListScan", "koreio_8h.html#a8b4fa96d26d0ecf71dab72279bb890b5", null ],
    [ "kio_i2c_StartRead", "koreio_8h.html#ab0350a42088da06d3dfb1ce81a3ab70f", null ],
    [ "kio_i2c_StartScan", "koreio_8h.html#a0d90cb709426547b3afc689fc9e5a4b9", null ],
    [ "kio_i2c_Write", "koreio_8h.html#a52e25f40eaa87fde9351c1db824b4f61", null ],
    [ "kio_ReadAnalog", "koreio_8h.html#af41ec56e036ceca45ed262f28687e40a", null ],
    [ "kio_ReadCAN", "koreio_8h.html#ac25b794aa3b941afe0c92ba48e32be4b", null ],
    [ "kio_ReadIO", "koreio_8h.html#a728096153bd62def5557c25059d2b3ca", null ],
    [ "kio_SendCAN", "koreio_8h.html#a19d86c8e3b1eefe2e308c8d14be994b0", null ],
    [ "kio_SetANValue", "koreio_8h.html#ab57ed0ac78e87f47595b64cf7badf9eb", null ],
    [ "kio_SetIO", "koreio_8h.html#a4e96c3878ce5681cb6e7498da44ef169", null ],
    [ "kio_t2c_ReturnRead", "koreio_8h.html#a971a6223181241a5dda2fdd35e23483a", null ]
];