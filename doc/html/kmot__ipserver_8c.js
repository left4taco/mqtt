var kmot__ipserver_8c =
[
    [ "DEFAULTPORT", "kmot__ipserver_8c.html#a3db4dba8d56385b62759b89c98cae447", null ],
    [ "ERRORSTRING", "kmot__ipserver_8c.html#a674a9e55fea7f57dec96f5369b6f76d0", null ],
    [ "NMOTOR", "kmot__ipserver_8c.html#aeeed8a2bf68f0b3ea92fd7bbfae1c8cc", null ],
    [ "RCVBUFSIZE", "kmot__ipserver_8c.html#a757e6521ee579cf97718d37b81972cd5", null ],
    [ "SENDBUFFERSIZE", "kmot__ipserver_8c.html#a2b3b13f7549f70267d828e8f6b1aedb5", null ],
    [ "kmot_ipConfigPID", "kmot__ipserver_8c.html#aeb7a7e74ac62cf47c6448cba4c2e6b05", null ],
    [ "kmot_ipFirmware", "kmot__ipserver_8c.html#a55ec5039f6e6d09b9ea9301633857103", null ],
    [ "kmot_ipInitMotor", "kmot__ipserver_8c.html#aefee70b299e8cc951cba9a7035ed4316", null ],
    [ "kmot_ipMeasure", "kmot__ipserver_8c.html#aa089208ba8644361e8c592dbf9575b13", null ],
    [ "kmot_ipResetError", "kmot__ipserver_8c.html#a4694f087cc69d5d7f31356fb72b6c2ee", null ],
    [ "kmot_ipSetPointSource", "kmot__ipserver_8c.html#a6199d23dfd07a575c74cac030905021c", null ],
    [ "kmot_ipSetPos", "kmot__ipserver_8c.html#a098762f377f3744e39dcda90464d367e", null ],
    [ "kmot_ipSetSpeed", "kmot__ipserver_8c.html#ab8a633bc2dd4f51cd2d2e822e105e2ec", null ],
    [ "kmot_ipStatus", "kmot__ipserver_8c.html#aa2c0d9b91549489e470e976046e86915", null ],
    [ "main", "kmot__ipserver_8c.html#a0ddf1224851353fc92bfbff6f499fa97", null ],
    [ "motorList", "kmot__ipserver_8c.html#a31e73e161a5475a83f1dad460760289d", null ],
    [ "sendBuffer", "kmot__ipserver_8c.html#a994af6823e734c8f255d5041a1c03018", null ]
];