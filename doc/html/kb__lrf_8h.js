var kb__lrf_8h =
[
    [ "LRF_DATA_NB", "kb__lrf_8h.html#ab1407e94ccf42add95882020034824b9", null ],
    [ "kb_lrf_Close", "kb__lrf_8h.html#a700ca92fee4a6133fe5122c922ea37f3", null ],
    [ "kb_lrf_Get_Timestamp", "kb__lrf_8h.html#ae7a201d4cbc8f63298ac5473c8727108", null ],
    [ "kb_lrf_GetDistances", "kb__lrf_8h.html#a90eac9b211d77a906e9e01e54df4c566", null ],
    [ "kb_lrf_GetDistances_Averaged", "kb__lrf_8h.html#a75885eab9b9161879a45555528e47ab3", null ],
    [ "kb_lrf_Init", "kb__lrf_8h.html#a16ddc8caf23b78afcfbbeaacfce48a62", null ],
    [ "kb_lrf_Laser_Off", "kb__lrf_8h.html#a26cd6f6dfd0b98fa3c8156a3638640e4", null ],
    [ "kb_lrf_Laser_On", "kb__lrf_8h.html#a8b9eaa2b056c0f3413d25d1469212b79", null ],
    [ "kb_lrf_Power_Off", "kb__lrf_8h.html#a645908f867abd8fa6dee590c1391284a", null ],
    [ "kb_lrf_Power_On", "kb__lrf_8h.html#a59e2e82056874767c758e85b40257129", null ],
    [ "kb_lrf_DistanceData", "kb__lrf_8h.html#a24760cfb5cdb7b2edd5689982af47fae", null ]
];