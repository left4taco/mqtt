var annotated =
[
    [ "_cel", "struct__cel.html", "struct__cel" ],
    [ "_fifo", "struct__fifo.html", "struct__fifo" ],
    [ "buffer", "structbuffer.html", "structbuffer" ],
    [ "i2c_t", "structi2c__t.html", "structi2c__t" ],
    [ "kb_alias_config_s", "structkb__alias__config__s.html", "structkb__alias__config__s" ],
    [ "kb_command_s", "structkb__command__s.html", "structkb__command__s" ],
    [ "kb_device_config_s", "structkb__device__config__s.html", "structkb__device__config__s" ],
    [ "kb_register_config_s", "structkb__register__config__s.html", "structkb__register__config__s" ],
    [ "kb_section_config_s", "structkb__section__config__s.html", "structkb__section__config__s" ],
    [ "kb_symbol_s", "structkb__symbol__s.html", "structkb__symbol__s" ],
    [ "kb_table_s", "structkb__table__s.html", "structkb__table__s" ],
    [ "knet_bus_s", "structknet__bus__s.html", "structknet__bus__s" ],
    [ "knet_dev_s", "structknet__dev__s.html", "structknet__dev__s" ],
    [ "knet_rs232_s", "structknet__rs232__s.html", "structknet__rs232__s" ],
    [ "ksock_s", "structksock__s.html", "structksock__s" ],
    [ "PWM_Value_t", "structPWM__Value__t.html", "structPWM__Value__t" ],
    [ "urg_state_t", "structurg__state__t.html", "structurg__state__t" ]
];