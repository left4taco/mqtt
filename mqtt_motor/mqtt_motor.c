#include "signal.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "paho/MQTTClient.h"
#include "khepera/khepera.h"
#include "cJSON.h"

#define ADDRESS     argv[1]
#define CLIENTID    argv[2]
#define TOPIC       argv[2]
#define QOS         1
#define TIMEOUT     10000L

volatile MQTTClient_deliveryToken deliveredtoken;
volatile double left_wheel_speed = 0.0F; // in mm
volatile double right_wheel_speed = 0.0F; // in mm

char buffer[64];

static knet_dev_t * dsPic; // robot pic microcontroller access
static int quitReq = 0; // quit variable for loop

void delivered(void *context, MQTTClient_deliveryToken dt)
{
    printf("Message with token value %d delivery confirmed\n", dt);
    deliveredtoken = dt;
}

int msgarrvd(void *context, char *topicName, int topicLen, MQTTClient_message *message)
{
    int i;
    char* payloadptr;

    printf("Message arrived\n");
    printf("\ttopic: %s\n", topicName);
    printf("\tmessage: ");

    payloadptr = message->payload;
    for(i=0; i<message->payloadlen; i++)
    {
        buffer[i] = *payloadptr;
        // putchar(*payloadptr);
        payloadptr++;
    }
    buffer[message->payloadlen] = '\0';
    printf("(from buffer) %s", buffer);
    putchar('\n');
    printf("\tlength: %d\n", strlen(buffer));

    cJSON *speed_data = cJSON_Parse(buffer);
    if (speed_data == NULL)
    {
      printf("Message not json.");
      return;
    }else{
      left_wheel_speed = cJSON_GetObjectItem(speed_data, "l")->valuedouble;
      right_wheel_speed = cJSON_GetObjectItem(speed_data, "r")->valuedouble;
      kh4_set_speed((long)(left_wheel_speed/KH4_SPEED_TO_MM_S) ,(long)(right_wheel_speed/KH4_SPEED_TO_MM_S) ,dsPic);
    }

    MQTTClient_freeMessage(&message);
    MQTTClient_free(topicName);
    return 1;
}

void connlost(void *context, char *cause)
{
  printf("\nConnection lost\n");
  printf("     cause: %s\n", cause);
}



/*--------------------------------------------------------------------*/
/*!
 * Make sure the program terminate properly on a ctrl-c
 */
static void ctrlc_handler( int sig )
{
  quitReq = 1;

  kh4_set_speed(0 ,0 ,dsPic); // stop robot
  kh4_SetMode( kh4RegIdle,dsPic );

  kh4_SetRGBLeds(0,0,0,0,0,0,0,0,0,dsPic); // clear rgb leds because consumes energy

  kb_change_term_mode(0); // revert to original terminal if called

  exit(0);
}

int main(int argc, char *argv[]) {
  if(argc < 3){
    printf("Usage: ./mqtt_motor broker_ip robot_id\nExample: ./mqtt_motor 127.0.0.1 robot_1");
  }
  // MQTT part
  MQTTClient client;
  MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
  int rc;
  int ch;
  MQTTClient_create(&client, ADDRESS, CLIENTID,
      MQTTCLIENT_PERSISTENCE_NONE, NULL);
  conn_opts.keepAliveInterval = 20;
  conn_opts.cleansession = 1;
  MQTTClient_setCallbacks(client, NULL, connlost, msgarrvd, delivered);
  if ((rc = MQTTClient_connect(client, &conn_opts)) != MQTTCLIENT_SUCCESS)
  {
    printf("Failed to connect, return code %d\n", rc);
    exit(EXIT_FAILURE);
  }
  printf("Subscribing to topic %s\nfor client %s using QoS%d\n\n"
  "Press Q<Enter> to quit\n\n", TOPIC, CLIENTID, QOS);
  MQTTClient_subscribe(client, TOPIC, QOS);

  // Khepera part
  double fpos,dval;
  int lpos,rpos;
  char Buffer[100],bar[12][64],revision,version;
  int i,n,val,type_of_test=0,sl,sr,pl,pr,mean;
  short index, value,sensors[12],usvalues[5];
  char c;
  long motspeed;
  char line[80],l[9];
  int kp,ki,kd;
  int pmarg,maxsp,accinc,accdiv,minspacc, minspdec; // SetSpeedProfile

  // initiate libkhepera and robot access
  if ( kh4_init(0,NULL)!=0)
  {
    printf("\nERROR: could not initiate the libkhepera!\n\n");
    return -1;
  }

  /* open robot socket and store the handle in its pointer */
  dsPic  = knet_open( "Khepera4:dsPic" , KNET_BUS_I2C , 0 , NULL );

  if ( dsPic==NULL)
  {
  	printf("\nERROR: could not initiate communication with Kh4 dsPic\n\n");
  	return -2;
  }

  /* ------  initialize the motors controlers --------------------------------*/

  /* tuned parameters */
  pmarg=20;
  kh4_SetPositionMargin(pmarg,dsPic ); 				// position control margin
  kp=10;
  ki=5;
  kd=1;
  kh4_ConfigurePID( kp , ki , kd,dsPic  ); 		// configure P,I,D

  accinc=3;
  accdiv=0;
  minspacc=20;
  minspdec=1;
  maxsp=400;
  kh4_SetSpeedProfile(accinc,accdiv,minspacc, minspdec,maxsp,dsPic ); // Acceleration increment ,  Acceleration divider, Minimum speed acc, Minimum speed dec, maximum speed

  kh4_SetMode( kh4RegIdle,dsPic );  				// Put in idle mode (no control)

  // get revision
  if(kh4_revision(Buffer, dsPic)==0){
    version=(Buffer[0]>>4) +'A';
    revision=Buffer[0] & 0x0F;
    printf("\r\nVersion = %c, Revision = %u\r\n",version,revision);
  }

  signal( SIGINT , ctrlc_handler ); // set signal for catching ctrl-c

  //  ------  battery information --------------------------------------------------
  kh4_battery_status(Buffer,dsPic);
  printf("\nBattery:\n  status (DS2781)   :  0x%x\n",Buffer[0]);
  printf("  remaining capacity:  %4.0f mAh\n",(Buffer[1] | Buffer[2]<<8)*1.6);
  printf("  remaining capacity:   %3d %%\n",Buffer[3]);
  printf("  current           : %5.0f mA\n",(short)(Buffer[4] | Buffer[5]<<8)*0.07813);
  printf("  average current   : %5.0f mA\n",(short)(Buffer[6] | Buffer[7]<<8)*0.07813);
  printf("  temperature       :  %3.1f C \n",(short)(Buffer[8] | Buffer[9]<<8)*0.003906);
  printf("  voltage           :  %4.0f mV \n",(Buffer[10] | Buffer[11]<<8)*9.76);
  printf("  charger           :  %s\n",kh4_battery_charge(dsPic)?"plugged":"unplugged");

  // Tell to the motor controller to move the Khepera 4 backward, in speed profile control
  kh4_SetMode( kh4RegSpeedProfile,dsPic );

  kh4_set_speed((long)(left_wheel_speed/KH4_SPEED_TO_MM_S) ,(long)(right_wheel_speed/KH4_SPEED_TO_MM_S) ,dsPic);
  // Forever loop
  do
  {
      ch = getchar();
  } while(ch!='Q' && ch != 'q');
  MQTTClient_disconnect(client, 10000);
  MQTTClient_destroy(&client);
	kh4_set_speed(0.0F ,0.0F ,dsPic);

  return 0;
}

